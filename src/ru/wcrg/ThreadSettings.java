package ru.wcrg;

/**
 * Created by Эдуард on 09.12.2017.
 */
    public interface ThreadSettings {
        int CLIENT_SLEEP_TIME = 25;
        int SERVICE_SLEEP_TIME = 100;
    }
