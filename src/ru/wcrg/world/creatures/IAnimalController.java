package ru.wcrg.world.creatures;

/**
 * Created by Эдуард on 28.12.2017.
 */
public interface IAnimalController {
    public void OnAnimalDied(Animal animal);
}
